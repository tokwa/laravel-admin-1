@extends('layouts.admin.admin')

@section('admin-content')
<div class="breadcrumbs">
    <div class="breadcrumbs-inner">
        <div class="row m-0">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Table</a></li>
                            <li class="active">Data table</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Data Table | </strong>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-md btn-outline-success ti-user" data-toggle="modal"
                            data-target="#createuser">
                            Create User
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="createuser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h5 class="modal-title" id="exampleModalLabel">Create New User</h5>
                                    </div>
                                    <div class="modal-body">
                                        <div class="container-fluid bg-danger mb-3 text-center" id="genErrBox">
                                            <strong><span id="msg" style="color:white;"></span></strong>
                                        </div>
                                        <form method="POST" action="" id="createForm">
                                            @csrf

                                            <div class="form-group row">
                                                <label for="name" class="col-md-4 col-form-label text-md-right">{{
                                                    __('Name') }}</label>

                                                <div class="col-md-6">
                                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"
                                                        autofocus>
                                                    <div class="invalid-feedback" id="err-name" role="alert"></div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="email" class="col-md-4 col-form-label text-md-right">{{
                                                    __('E-Mail Address') }}</label>

                                                <div class="col-md-6">
                                                    <input id="email" type="text" class="form-control" name="email"
                                                        value="{{ old('email') }}">
                                                    <div class="invalid-feedback" id="err-email" role="alert"></div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="role" class="col-md-4 col-form-label text-md-right">Role</label>

                                                <div class="col-md-6">
                                                    <select id="role-id" name="role_id" class="form-control">
                                                        <option value="1">User</option>
                                                        <option value="2">Moderator</option>
                                                        <option value="3">Admin</option>
                                                        <option value="4">Super Admin</option>
                                                    </select>
                                                    <div class="invalid-feedback" id="err-role" role="alert"></div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="password" class="col-md-4 col-form-label text-md-right">{{
                                                    __('Password') }}</label>

                                                <div class="col-md-6">
                                                    <input id="password" type="password" class="form-control" name="password">
                                                    <div class="invalid-feedback" id="err-password" role="alert"></div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{
                                                    __('Confirm Password') }}</label>

                                                <div class="col-md-6">
                                                    <input id="password-confirm" type="password" class="form-control"
                                                        name="password_confirmation">
                                                    <div class="invalid-feedback" id="err-password-confirm" role="alert"></div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                        <button id="submitBtn" type="submit" class="btn btn-primary">
                                            {{ __('Register') }}
                                        </button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Role</th>
                                    <th>Status</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Member Since</th>
                                    <th>Created At</th>
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($users))
                                @foreach($users as $user)
                                <tr>
                                    <td>{{$x++}}</td>
                                    <td>
                                        @if ($user->role_id == 1)
                                        {{'user'}}
                                        @elseif ($user->role_id == 2)
                                        {{'moderator'}}
                                        @elseif ($user->role_id == 3)
                                        {{'admin'}}
                                        @elseif ($user->role_id == 4)
                                        {{'super admin'}}
                                        @else
                                        {{'invalid role'}}
                                        @endif
                                    </td>
                                    <td>
                                        @if ($user->email_verified_at)
                                        <span class="text-primary">{{'active'}}</span>
                                        @else
                                        <span class="text-danger">{{'inactive'}}</span>
                                        @endif
                                    </td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>
                                        @if (!$user->email_verified_at)
                                        <span class="text-danger">{{'-- -- --'}}</span>
                                        @else
                                        <span class="text-primary text-center">{{$user->email_verified_at}}</span>
                                        @endif
                                    </td>
                                    <td>{{$user->created_at}}</td>
                                    <td>
                                        <!-- Button trigger modal -->
                                        <a href="#" class="btn btn-outline-success ti-pencil" data-toggle="modal"
                                            data-target="#edituser" data-editname="{{ $user->name }}" data-editemail="{{ $user->email }}"
                                            data-editroleid="{{ $user->role_id}}" data-url="{{ route('admin.update', $user->id) }}">
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
    <!-- Modal -->
    <div class="modal fade" id="edituser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                </div>
                <div class="modal-body">
                    <div class="container-fluid bg-danger mb-3 text-center" id="editErrorBox">
                        <strong><span id="editMsg" style="color:white;"></span></strong>
                    </div>
                    <form method="POST" action="" id="editForm">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{
                                __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="editname" type="text" class="form-control" name="name" autofocus>
                                <div class="invalid-feedback" id="err-editname" role="alert"></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{
                                __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="editemail" type="text" class="form-control" name="email">
                                <div class="invalid-feedback" id="err-editemail" role="alert"></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="role" class="col-md-4 col-form-label text-md-right">Role</label>

                            <div class="col-md-6">
                                <select id="editrole-id" name="role_id" class="form-control">
                                    <option value="1">User</option>
                                    <option value="2">Moderator</option>
                                    <option value="3">Admin</option>
                                    <option value="4">Super Admin</option>
                                </select>
                                <div class="invalid-feedback" id="err-editrole" role="alert"></div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button id="editBtn" type="submit" class="btn btn-primary">
                        {{ __('Update') }}
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div><!-- .content -->

<script src="{{ asset('js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>

<script src="{{asset('js/lib/data-table/datatables.min.js')}}"></script>
<script src="{{asset('js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/lib/data-table/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('js/lib/data-table/jszip.min.js')}}"></script>
<script src="{{asset('js/lib/data-table/vfs_fonts.js')}}"></script>
<script src="{{asset('js/lib/data-table/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/lib/data-table/buttons.print.min.js')}}"></script>
<script src="{{asset('js/lib/data-table/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/init/datatables-init.js')}}"></script>

{{-- <script type="text/javascript">
    $(document).ready(function () {
        $('#bootstrap-data-table-export').DataTable();
    });
</script> --}}

<script>
    document.getElementById('createForm').addEventListener('submit', (e) => {
        e.preventDefault()

        $('#submitBtn').html('<i class="fas fa-spinner fa-spin"></i> Uploading ..')
        $('#submitBtn').prop('disabled', true)

        const x = new FormData()
        x.append('name', document.getElementById('name').value)
        x.append('email', document.getElementById('email').value)
        x.append('role_id', document.getElementById('role-id').value)
        x.append('password', document.getElementById('password').value)
        x.append('password_confirmation', document.getElementById('password-confirm').value)

        axios.post("{{ route('admin.store') }}", x)

            .then((response) => {
                console.log(response.data)
                $('#genErrBox').removeClass('bg-danger')
                $('#genErrBox').addClass('bg-success')
                $('#msg').html(response.data.sms)
                window.location.href = "{{ route('admin.users') }}"
            })

            .catch((error) => {
                $('#submitBtn').html('Submit')
                $('#submitBtn').prop('disabled', false)
                console.log(error.response.data.errors.name)
                const errBoxes = document.getElementsByClassName('form-control')
                const errSms = document.getElementsByClassName('invalid-feedback')
                Array.from(errBoxes).forEach(el => el.classList.remove('is-invalid'))
                Array.from(errSms).forEach(el => el.innerHTML = "")
                Array.from(errSms).forEach(el => el.style.display = "none")

                if (error.response.data.errors.name) {
                    let box = document.querySelector('#name')
                    let sms = document.getElementById('err-name')
                    sms.innerHTML = error.response.data.errors.name[0]
                    sms.style.display = "block"
                    box.classList.add('is-invalid')
                }

                if (error.response.data.errors.email) {
                    let box = document.querySelector('#email')
                    let sms = document.getElementById('err-email')
                    sms.innerHTML = error.response.data.errors.email[0]
                    sms.style.display = "block"
                    box.classList.add('is-invalid')
                }

                if (error.response.data.errors.role_id) {
                    let box = document.querySelector('#role-id')
                    let sms = document.getElementById('err-role')
                    sms.innerHTML = error.response.data.errors.role_id[0]
                    sms.style.display = "block"
                    box.classList.add('is-invalid')
                }

                if (error.response.data.errors.password) {
                    let box = document.querySelector('#password')
                    let sms = document.getElementById('err-password')
                    sms.innerHTML = error.response.data.errors.password[0]
                    sms.style.display = "block"
                    box.classList.add('is-invalid')
                }

                if (error.response.data.errors.password_confirmation) {
                    let box = document.querySelector('#password-confirm')
                    let sms = document.getElementById('err-password-confirm')
                    sms.innerHTML = error.response.data.errors.password_confirmation[0]
                    sms.style.display = "block"
                    box.classList.add('is-invalid')
                }

                $('#msg').html(error.response.data.errors = "Enter a value to all fields!")
            })

    });


    $('#edituser').on('show.bs.modal', function (e) {
        var btn = $(e.relatedTarget)
        var myRole = btn.data('editroleid')
        var modal = $(this)
        var editname = modal.find('.modal-body #editname').val(btn.data('editname'))
        var editemail = modal.find('.modal-body #editemail').val(btn.data('editemail'))
        var editrole_id = modal.find('.modal-body #editrole-id').val(btn.data('editrole_id'))

        var test = modal.find('.modal-body #editForm').attr('action', btn.data('url'))

        if ($('#editrole-id').val(myRole)) {
            $("#editrole-id").val(myRole).attr('selected', true)
        }

        document.getElementById('editForm').addEventListener('submit', function (e) {
            e.preventDefault()

            $('#editBtn').html('<i class="fas fa-spinner fa-spin"></i> 업데이트 중 ..')
            $('#editBtn').prop('disabled', true)

            const x = {
                name: document.getElementById('editname').value,
                email: document.getElementById('editemail').value,
                role_id: document.getElementById('editrole-id').value
            }

            axios.put(this.action, x)
                .then((res) => {
                    // alert(res.data.sms)
                    $('#editErrorBox').removeClass('bg-danger')
                    $('#editErrorBox').addClass('bg-success')
                    $('#editMsg').html(res.data.sms)
                    window.location.reload()
                })

                .catch((error) => {
                    $('#editBtn').html('Submit')
                    $('#editBtn').prop('disabled', false)
                    // console.log(error.response.data.errors.name)
                    const errBoxes = document.getElementsByClassName('form-control')
                    const errSms = document.getElementsByClassName('invalid-feedback')
                    Array.from(errBoxes).forEach(el => el.classList.remove('is-invalid'))
                    Array.from(errSms).forEach(el => el.innerHTML = "")
                    Array.from(errSms).forEach(el => el.style.display = "none")

                    if (error.response.data.errors.name) {
                        let box = document.querySelector('#editname')
                        let sms = document.getElementById('err-editname')
                        sms.innerHTML = error.response.data.errors.name[0]
                        sms.style.display = "block"
                        box.classList.add('is-invalid')
                    }

                    if (error.response.data.errors.email) {
                        let box = document.querySelector('#editemail')
                        let sms = document.getElementById('err-editemail')
                        sms.innerHTML = error.response.data.errors.email[0]
                        sms.style.display = "block"
                        box.classList.add('is-invalid')
                    }

                    if (error.response.data.errors.role_id) {
                        let box = document.querySelector('#editrole-id')
                        let sms = document.getElementById('err-editrole')
                        sms.innerHTML = error.response.data.errors.role_id[0]
                        sms.style.display = "block"
                        box.classList.add('is-invalid')
                    }

                    $('#msg').html(error.response.data.errors = "Failed to update!")
                })

        });

    });
</script>
@endsection
