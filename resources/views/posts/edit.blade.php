@extends('layouts.app')

@section('content')
<div class="container">
    <form method="POST" id="editForm" action="" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="title">Post Title</label>
            <input type="text" class="form-control" id="title" name="title" aria-describedby="title-Help" value="{{$post->title}}">
            <div class="invalid-feedback" id="err-title" role="alert"></div>
            <small id="titleHelp" class="form-text text-muted">Title of the post.</small>
        </div>
        <div class="form-group">
            <label for="body">Post Body</label>
            <textarea class="form-control" id="body" name="body" rows="3">{{$post->body}}</textarea>
            <div class="invalid-feedback" id="err-body" role="alert"></div>
            <small id="titleHelp" class="form-text text-muted">Post content.</small>
        </div>
        <div class="form-group">
            <img class="img-fluid" src="{{'/images/posts/' . $post->image}}" alt="">
        </div>
        <div class="form-group">
            <input type="file" id="image" name="image" />
            <div class="invalid-feedback" id="err-image" role="alert"></div>
        </div>
        <div class="form-group row">
                {{-- POST TAGS --}}
                <div class="col-lg-12">
                    <strong class="text-muted mb-3 mt-4 d-block">Tags:</strong>
                    <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="checkbox" id="select_all" class="custom-control-input text-lowercase">
                        <label class="custom-control-label text-uppercase" for="select_all"><strong>select all</strong></label>
                    </div>
                    @foreach( $tags as $tag )
                    <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="checkbox" name="tags" class="custom-control-input text-lowercase row_check"
                            id="{{ "tag-" . utf8_decode($tag->name) }}" value="{{ $tag->id }}" data-id="{{$tag->id}}"
                            @foreach($post->tags as $x)
                                @if ($x->id == $tag->id)
                                        checked
                                @endif
                            @endforeach
                            >
                        <label class="custom-control-label text-lowercase" for="{{ "tag-". utf8_decode($tag->name) }}">
                        {{ utf8_decode($tag->name) }}</label>
                    </div>
                    @endforeach
                    <div class="invalid-feedback" id="err-tags" role="alert"></div>
                </div>
            </div>
        <div class="form-group">
            <a class="btn btn-danger" href="{{URL::previous()}}">Cancel</a>
            <button type="submit" id="submitBtn" class="btn btn-primary">
                {{ __('Submit') }}
            </button>
        </div>
    </form>
</div>

<script>

    $('#select_all').on('click', (e) => {        
        $('.row_check').prop('checked', $(e.target).prop('checked'));
    })

    $('.row_check').on('click', () => {
        if ($('.row_check:checked').length == $('.row_check').length) {
            $('#select_all').prop('checked', true)
        } else {
            $('#select_all').prop('checked', false)
        }
    })

    document.getElementById('editForm').addEventListener('submit', function (e) {

        e.preventDefault()

        $('#submitBtn').html('<i class="fas fa-spinner fa-spin"></i> Loading ..')
        $('#submitBtn').prop('disabled', true)

        var idsArr = [];

        $('.row_check:checked').each( function() {
            idsArr.push($(this).val())
        })

        var strIds = idsArr.join(',')

        console.log(strIds)

        const x = new FormData()
        x.append('title', document.getElementById('title').value)
        x.append('body', document.getElementById('body').value)
        x.append('image', document.getElementById('image').files[0])
        x.append('tags', strIds)
        x.append('_method', 'PUT')

        // console.log(x)

        axios.post("{{ route('posts.update', $post->id) }}", x)
            .then((res) => {
                alert(res.data.sms)
                window.location.href = "{{ route('posts.index') }}"
            })
            .catch((error) => {

                console.log(error.response)

                $('#submitBtn').html("<i class='fas fa-save'></i> Submit")
                $('#submitBtn').prop('disabled', false)
                // $('#cancelUpdate').prop('disabled', false)
                
                const errBoxes = document.getElementsByClassName('form-control')
                const errSms = document.getElementsByClassName('invalid-feedback')
                Array.from(errBoxes).forEach(el => el.classList.remove('is-invalid'))
                Array.from(errSms).forEach(el => el.innerHTML = "")
                Array.from(errSms).forEach(el => el.style.display = "none")

                if (error.response.data.errors.title) {
                    let box = document.querySelector('#title')
                    let sms = document.getElementById('err-title')
                    sms.innerHTML = error.response.data.errors.title[0]
                    sms.style.display = "block"
                    box.classList.add('is-invalid')
                }

                if (error.response.data.errors.body) {
                    let box = document.querySelector('#body')
                    let sms = document.getElementById('err-body')
                    sms.innerHTML = error.response.data.errors.body[0]
                    sms.style.display = "block"
                    box.classList.add('is-invalid')
                }

                if (error.response.data.errors.tags) {
                    let box = document.querySelector('#{{ "tag-" . utf8_decode($tag->name) }}')
                    let sms = document.getElementById('err-tags')
                    sms.innerHTML = error.response.data.errors.tags[0]
                    sms.style.display = "block"
                    box.classList.add('is-invalid')
                }

            })
    });
</script>

@endsection
