@extends('layouts.app')

@section('content')

@include('layouts.messages')

<div class="container">

    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <form action="{{route('posts.index')}}" method="GET">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">Search By</button>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Title</a>
                <a class="dropdown-item" href="#">Tag</a>
            </div>
        </div>
        <input type="text" class="form-control" name="s" placeholder=" Search..." value="{{ isset($s) ? $s : '' }}">
    </form>
    </div>

    <div class="row">
        @if(count($posts))
        @foreach($posts as $post)
        <div class="col-lg-4 col-md-4">
            <div class="card" style="width: ">
                <img class="card-img-top" src="{{'/images/posts/' . $post->image}}" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">{{$post->title}}</h5>
                    <small>
                        @foreach($post->tags as $x)
                        {{ $x->name }}
                        {{ !$loop->last ? ',' : '' }}
                        @endforeach
                    </small>
                    <br>
                    <small>Date: {{$post->created_at}}</small>
                    <br>
                    <small>By: {{$post->user->name}}</small>
                    <br><br>
                    <p class="card-text">
                        {{$post->body}}
                    </p>
                    <div class="form-group">
                    </div>
                    @can('admin_only', $post)
                    <div class="form-group">
                        <form method="POST" action="{{ route('posts.destroy', $post->id) }}">
                            <a href="{{route('posts.edit', $post->id)}}" class="btn btn-primary">Edit</a>
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </div>
                    @endcan
                </div>
            </div>
        </div>
        @endforeach
        @else
        <p>There is no post at the moment.</p>
        @endif
    </div>
    {{$posts->appends(['s' => $s])->links()}}
</div>
@endsection