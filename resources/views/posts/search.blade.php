@extends('layouts.app')

@section('content')
<div class="container">
        <form action="" method="GET">
            <div class="form-group">
            <input type="text" class="form-control" name="s" placeholder=" Search..." value="{{isset($s) ? : ''}}">
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="submit">
                    Search
                </button>
            </div>
        </form>
    </div>
        {{-- {{$posts->appends(['s' => $s])->links()}} --}}
@endsection