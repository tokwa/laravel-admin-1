<?php

namespace App\Http\Controllers;

use App\Post;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;


class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
        $this->middleware('auth')->except('index', 'show');
    }

    public function index(Request $request)
    {   
        $s = $request->input('s');

        $posts = Post::orderBy('created_at', 'desc')->search($s)->paginate('3');

        return view('posts.index', compact('posts', 's'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Tag $tags)
    {
        $tags = Tag::orderBy('created_at', 'desc')->get();

        return view('posts.create', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'title' => ['required'],
            'body' => ['required'],
            'image' => ['required', 'image', 'max:2048'],
            'tags' => ['required', 'exists:tags,id']
        ]);

        $post = new Post;
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->user_id = auth()->id();
        
        if ($request->hasFile('image')) {
            $imgName = md5(date('Y-m-d'). "_" . microtime()) . 
            "." . $request->file('image')->getClientOriginalExtension();
            $post->image = $imgName;
        }

         if ($post->save()) {
            $post->tags()->attach(explode(',',$request->input('tags')));
            $request->image->move(public_path('images/posts'), $imgName);
            $x = session()->flash('success','Post created!');

            return response()->json([
                        'status' => 200,
                        'sms' => 'Post Created!',
                        'x'
                    ]);
        }   else {
            return response()->json([
                'status' => 500,
                'sms' => 'Something went wrong!'
            ]);
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $tags = Tag::get();

        if (Gate::allows('posts.edit', $post)) {
            return view('posts.edit', compact('post','tags'));
        }
        else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $post)
    {
        if (! request()->ajax() && 
        !request()->isSecure()) {
            return response()->json(['error' => 'Failed to update.'], 500);
        }

        $request->validate([
            'title' => ['required'],
            'body' => ['required'],
            'tags' => ['required', 'exists:tags,id']
        ]);

        if ($request->hasFile('image')) {
            $request->validate([
                'image' => ['image', 'max:2048']  
            ]);

            $imgName = md5(date('Y-m-d'). "_" . microtime()) .
            "." . $request->file('image')->getClientOriginalExtension();
        }

            $x = Post::findOrFail($post);
            $x->title = $request->title;
            $x->body = $request->body;
        
        if ($request->hasFile('image')) {
            $x->image = $imgName;
        }

        if ($x->save()) {
            
            if ($request->hasFile('image')) {
                $request->image->move(public_path('images/posts'), $imgName);
            }

            $x->tags()->detach();
            $x->tags()->attach(explode(',',$request->input('tags')));
            $x = session()->flash('success','Post Updated!');

            return response()->json([
                'status' => 200,
                'sms' => 'Successfully Edited!!',
                'x'
            ]);
        } 
        else {
            return response()->json([
                'status' => 500,
                'sms' => 'Something went wrong!!'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if (Gate::allows('posts.edit', $post)) {

            $post->tags()->detach();

            $post->delete($post);
            
            return redirect()->route('posts.index')->with('success', 'Post Deleted!');
        }
        else {
            abort(404);
        }
        

    }
}
