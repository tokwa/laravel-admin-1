<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller
{
    // USER CONTROLLERS

    public function index() {

        return view('admin.index');
    }

    public function users(User $users) {

        $users = User::orderBy('created_at', 'desc')->get();
        $x = 1;
        
        return view('admin.users', compact('users', 'x'));
    }

    public function store(Request $request) {

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'role_id' => ['required','exists:roles,id', 'in:1,2,3,4'],
        ]);

        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->role_id = $request->input('role_id');
        
        if ($user->save()) {

            return response()->json([
                'status' => 200,
                'sms' => 'User successfully created.'
            ]);
        }
        else {
            return response()->json([
                'status' => 500,
                'sms' => 'Something went wrong =('
            ]);
        }
    }

    public function edit(User $user) {

        return view('admin.users', compact('user'));
    }

    public function update(Request $request, $user) {

        if (! request()->ajax() && 
        !request()->isSecure()) {
            return response()->json(['error' => 'Failed to update.'], 500);
        }

        $request->validate([
            'name' => ['required'],
            'email' => ['required', 'email', 'unique:users,email,' . $user],
            'role_id' => ['required','exists:roles,id', 'in:1,2,3,4'],
        ]);

        $user = User::where('id', $user)->update([
            'name' => $request->name,
            'email' => $request->email,
            'role_id' => $request->role_id
        ]);

        if ($user) {

            return response()->json([
                'status' => 200,
                'sms' => 'Successfully Edited!!'
            ]);
        } else {
            return response()->json([
                'status' => 500,
                'sms' => 'Something went wrong!!'
            ]);
        }
    }
    
}
