<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Auth;
use App\Post;
use App\Policies\PostPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
         Post::class => PostPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        
        Gate::define('posts.edit', 'App\Policies\PostPolicy@admin_only');

        // Gate::define('posts.edit', function ($user, $post) {

        //     if (Auth::check() && Auth::user()->role_id == 3 || Auth::user()->role_id == 4) {
        //         return $user = true;
        //     }
        //     return $user->id == $post->user_id;
        // });


    }
}
