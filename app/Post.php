<?php

namespace App;
use App\Tag;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title', 'body', 'image'];

    public function user() {

        return $this->belongsTo('App\User', 'user_id');
    }

    public function tags() {

        return $this->belongsToMany('App\Tag', 'tag_post');
    }

    public function scopeSearch($query, $s) {

        return $query->where('title', 'like', '%' .$s. '%')
            ->orWhere('body', 'like', '%' .$s. '%');
    }

}
