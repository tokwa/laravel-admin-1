<?php

namespace App\Policies;

use App\User;
use Auth;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function admin_only($user, $post) {

        if (Auth::check() && Auth::user()->role_id == 3 || Auth::user()->role_id == 4) {
                return $user = true;
            }
                return $user->id == $post->user_id;
        }
}
