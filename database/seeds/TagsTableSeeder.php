<?php

use Illuminate\Database\Seeder;
use App\Tag;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $x = ['bonus', 'tournament', 'uncategorized'];

    public function run()
    {
        for ($i = 0; $i < count($this->x); $i++) {
            Tag::create([
                'name' => $this->x[$i]
            ]);
        }
    }
}
