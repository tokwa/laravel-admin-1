<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $x = ['user', 'moderator', 'admin', 'superadmin'];

    public function run()
    {
        for ($i = 0; $i < count($this->x); $i++) {
            Role::create([
                'name' => $this->x[$i]
            ]);
        }
    }
}
